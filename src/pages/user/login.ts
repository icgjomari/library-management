import { Component } from "@angular/core";
import { ViewController, NavController } from 'ionic-angular';
import { Register } from './register';
import { User as UserModel } from '../../app/models/User';

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class Login {

    constructor(public navCtrl: NavController, private userModel: UserModel)
    {
    }

    login()
    {
        console.log( this.userModel.getDatas() );
    }

    register()
    {
        return this.navCtrl.push( Register );
    }

}