import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

/* --- Pages --- */
import { AboutPage } from '../pages/about/about';
import { Login } from '../pages/user/login';
import { Register } from '../pages/user/register';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
/* --- Pages --- */

/* --- Custom Component --- */
import { Navigation } from './components/navigation/navigation';
/* --- Custom Component --- */
import { User as UserModel } from './models/User';
import { UserB as UserModelB } from './models/UserB';

@NgModule({
  declarations: [
    /* --- Pages --- */
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Register,
    /* --- Pages --- */

    /* --- Custom Component --- */
    Navigation,
    /* --- Custom Component --- */
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    Login,
    TabsPage,
    Register,
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserModel,
  ]
})
export class AppModule {
  constructor()
  {
  }
}
