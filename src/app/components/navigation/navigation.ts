import { Component, ElementRef, Renderer } from "@angular/core";
import { NavController, ViewController, IonicApp } from 'ionic-angular';
import { MyApp } from '../../app.component';

@Component({
  selector: 'main-navigation',
  templateUrl: 'navigation.html',
})
export class Navigation
{
    private title: string;
    constructor(public element: ElementRef)
    {
        let title = element.nativeElement.attributes.title;
        if( title )
        {
            this.title = title.value;
        }
    }
    setTitle(title)
    {
        this.title = title;
    }
}