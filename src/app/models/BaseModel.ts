import { BaseModelInterface as Interface } from "./BaseModelInterface";

export class BaseModel implements Interface
{
    fillable: String[]
    datas: Object[]

    constructor()
    {
        
    }

    addData( data: any )
    {
        this.datas.push( data )
    }

    getDatas()
    {
        return this.datas
    }
}