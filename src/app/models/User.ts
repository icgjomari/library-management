import { Injectable } from '@angular/core';
import { BaseModel } from './BaseModel';

@Injectable()
export class User extends BaseModel
{
    public fillable = [
        "username",
        "last_name",
        "first_name",
        "password"
    ];

    public datas = [];

    constructor()
    {
        super()
    }

}