export interface BaseModelInterface
{
    fillable: String[]
    datas: Object[]
    addData: ( data: any ) => void
    getData?: () => {}
}